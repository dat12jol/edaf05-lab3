import java.io.File;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        HashSet<String> checkSet = new HashSet<String>();
        HashSet<String> visitedCities = new HashSet<String>();
        HashMap<String, ArrayList<Edge>> cityMap = new HashMap<String, ArrayList<Edge>>();
        PriorityQueue<Edge> queue = new PriorityQueue<Edge>();
        Scanner s= null;
        int distanceOfSpanningTree = 0;
        Long start = System.currentTimeMillis();
        try {
            s = new Scanner(new File("C:\\Users\\fjubb\\IdeaProjects\\lab2\\src\\usapls.txt"));
        }catch (Exception e){
            System.out.println("fjiesojfesoijfes");
        }
        boolean readEdge = false;
        while(s.hasNext()){
            String line = s.nextLine();
            if(!readEdge && line.contains("--")){
                readEdge = true;
            }
            if(!readEdge){
                checkSet.add(line.trim());
                cityMap.put(line, new ArrayList<Edge>());
            }
            if(readEdge){
                String[] cityData = line.trim().split("--");
                String cityFrom = cityData[0];
                String cityTo = cityData[1].substring(0, cityData[1].indexOf('[') - 1);
                int distance = Integer.valueOf(cityData[1].substring(cityData[1].indexOf('[')+1, cityData[1].length() - 1));
                cityMap.get(cityFrom).add(new Edge(cityFrom, cityTo, distance));
                cityMap.get(cityTo).add(new Edge(cityTo, cityFrom, distance));
            }
        }
        String initialCity = checkSet.iterator().next();
        checkSet.remove(initialCity);
        queue.addAll(cityMap.get(initialCity));
        visitedCities.add(initialCity);
        while(!checkSet.isEmpty()){
            Edge e = queue.remove();
            if(!visitedCities.contains(e.getCityTo())){
                visitedCities.add(e.getCityTo());
                checkSet.remove(e.getCityTo());
                distanceOfSpanningTree += e.getDistance();
                System.out.println(e.toString());
                queue.addAll(cityMap.get(e.getCityTo()));
            }
        }
        System.out.println(distanceOfSpanningTree + " miles, calculated in " + (System.currentTimeMillis() - start) + " ms");
    }
}
