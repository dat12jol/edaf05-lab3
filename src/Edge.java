import java.util.Comparator;

/**
 * Created by fjubb on 2016-05-13.
 */
public class Edge implements Comparable<Edge>{
    private String cityTo, cityFrom;
    private int distance;


    public Edge(String cityFrom, String cityTo, int distance){
        this.cityTo = cityTo;
        this.cityFrom = cityFrom;
        this.distance = distance;
    }
    public int getDistance(){
        return distance;
    }
    public String getCityTo() {
        return cityTo;
    }

    public int compareTo(Edge e) {
        return this.distance - e.getDistance();
    }
    public String toString(){
        return cityFrom + " " + cityTo + " " + distance;
    }
}
